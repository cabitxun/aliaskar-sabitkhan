<?php

# Task 1
$number = -1;

if ($number < 0) {
    echo "Число отрицательное" . PHP_EOL;
} else {
    echo "Число положительное" . PHP_EOL;
}


#Task 2

$str1 = "Hello World!";

echo strlen($str1) . PHP_EOL;


#Task 3

$str2 = "kurumun";
echo strpos($str2, "a") ? "да" . PHP_EOL : "нет" . PHP_EOL;

#Task 4

$number2 = 165;

if ($number2 % 2 == 0 && $number2 % 3 == 0 && $number2 % 5 == 0 && $number2 % 6 == 0 && $number2 % 9 == 0) {
    echo "Correct" . PHP_EOL;
} else {
    echo "Error" . PHP_EOL;
}


# Task 5

if ($number2 % 3 == 0 && $number2 % 5 == 0 && $number2 % 7 == 0 && $number2 % 11 == 0) {
    echo "Correct" . PHP_EOL;
} else {
    echo "Error" . PHP_EOL;
}


# Task 6

$str3 = "Aliaskar";

echo $str3[strlen($str3)-1] . PHP_EOL;


# Task 7

$str3 = "Aliaskar";

echo $str3[strlen($str3)-1] . PHP_EOL;


# Task 8

function triangleArea($a, $h) {
    return (1 / 2) * $a * $h;
}

$a = 5;
$h = 7;
echo triangleArea($a, $h) . PHP_EOL;


# Task 9
function squareArea($a, $b) {
    return $a * $b;
}

$a2 = 5;
$b = 7;
echo squareArea($a, $b) . PHP_EOL;


# Task 10

$number3 = 10;

echo pow($number3, 2) . PHP_EOL;